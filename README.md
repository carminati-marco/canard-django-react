# Django & React application

This is a docker structure with django & react js containers.
The project calls an external library, https://gitlab.com/carminati-marco/canard-poster, in order to have models, graphQL and APIs to work on.

## Running

Just launch `make build`
and you will see

- [http://127.0.0.1:80](http://127.0.0.1:80) is the Django app
- [http://127.0.0.1:3000](http://127.0.0.1:3000) is the React app

[note: containers use postgresql port; change the port in docker-compose.yml or switch off your postgresql server]

## Other commands

If you want to stop and remove the containers `make clean`.

If you want to access to the backend shell `make bash_backend`.

The others docker-compose & docker commands are still available

### Backend tests

Connect to the backend (`make bash_backend`) and launch `py.test`
