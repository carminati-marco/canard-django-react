clean:
	docker-compose kill && docker-compose rm --force

bash_backend:
	docker-compose exec backend bash

build: clean
	docker-compose build
	docker-compose up -d
	docker-compose exec backend sh init_django.sh 
