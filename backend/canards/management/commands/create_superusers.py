from django.conf import settings
from django.contrib.auth.models import User
from django.core.management.base import BaseCommand


class Command(BaseCommand):

    def handle(self, *args, **options):
        if not User.objects.exists():
            for user in settings.ADMINS:
                username = user[0].replace(' ', '')
                email = user[1]
                password = 'password'
                print('Creating account for %s (%s)' % (username, email))
                admin = User.objects.create_superuser(
                    email=email, username=username, password=password)
                admin.is_active = True
                admin.is_admin = True
                admin.save()
            print('Admin accounts created!')
        else:
            print('Admin accounts NOT created. Check if they exist')
