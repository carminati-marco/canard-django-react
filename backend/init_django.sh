#!/usr/bin/env bash

python manage.py migrate
python manage.py create_superusers
python manage.py init_fixtures_posters 300