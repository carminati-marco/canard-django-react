import React, { Component } from "react";
import { withStyles } from "@material-ui/core/styles";
import Card from "@material-ui/core/Card";
import CardHeader from "@material-ui/core/CardHeader";
import CardContent from "@material-ui/core/CardContent";
import Avatar from "@material-ui/core/Avatar";
import IconButton from "@material-ui/core/IconButton";
import Typography from "@material-ui/core/Typography";

const styles = theme => ({
  card: {
    width: 300,
    height: 450,
    margin: 5
  }
});

class CardDetail extends Component {
  render() {
    const { classes, poster } = this.props;
    return (
      <Card className={classes.card}>
        <CardHeader
          avatar={
            <Avatar aria-label={poster.title}>{poster.title.charAt(0)}</Avatar>
          }
          action={<IconButton />}
          title={poster.title}
          subheader={poster.year}
        />
        <CardContent>
          <Typography component="div">
            <img src={poster.posterUrl} alt={poster.title} />
            {poster.genre.split("|").map(genre => (
              <Typography component="div">{genre}</Typography>
            ))}
          </Typography>
        </CardContent>
      </Card>
    );
  }
}

export default withStyles(styles)(CardDetail);
