import React, { Component } from "react";
import { withStyles } from "@material-ui/core/styles";
import CardDetail from "./CardDetail";

const styles = () => ({
  container: {
    marginTop: 30,
    display: "flex",
    flexWrap: "wrap"
  }
});

class CardList extends Component {
  render() {
    const { classes, posters } = this.props;
    return (
      <div className={classes.container}>
        {posters.map(poster => {
          console.log("poster.node", poster.node);
          return (
            <div key={poster.node.id}>
              <CardDetail poster={poster.node} />
            </div>
          );
        })}
      </div>
    );
  }
}

export default withStyles(styles)(CardList);
