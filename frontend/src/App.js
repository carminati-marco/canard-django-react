import React, { Component } from "react";
import { withStyles } from "@material-ui/core/styles";
import SearchField from "react-search-field";
import "./App.css";
import gql from "graphql-tag";
import { ApolloProvider } from "react-apollo";

import ApolloClient from "apollo-boost";
import CardList from "./components/CardList";
import TypeChecker from "typeco";
import Typography from "@material-ui/core/Typography";

const client = new ApolloClient({
  uri: "http://localhost/graphql"
});

const styles = theme => ({
  search: {
    margin: 5
  }
});

const GET_POSTERS = gql`
  query Posters {
    posters {
      edges {
        node {
          id
          title
          posterUrl
          year
          genre
        }
      }
    }
  }
`;
const getMatchedList = (searchText, list) => {
  if (TypeChecker.isEmpty(searchText)) return list;
  return list.filter(
    item =>
      item.node.title.toUpperCase().indexOf(searchText.toUpperCase()) !== -1
  );
};

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      list: [],
      filteredList: []
    };
    this.onBasicExampleChange = this.onBasicExampleChange.bind(this);
  }

  onBasicExampleChange(value) {
    const posters = getMatchedList(value, this.state.list);
    this.setState({
      filteredList: posters
    });
  }

  componentDidMount() {
    client.query({ query: GET_POSTERS }).then(result => {
      this.setState({
        list: result.data.posters.edges,
        filteredList: result.data.posters.edges
      });
    });
  }

  render() {
    const { classes } = this.props;
    return (
      <ApolloProvider client={client}>
        <Typography component="div">
          <Typography component="div" className={classes.search}>
            <SearchField
              placeholder="Search item"
              onChange={this.onBasicExampleChange}
            />
          </Typography>
          <CardList posters={this.state.filteredList} />;
        </Typography>
      </ApolloProvider>
    );
  }
}

export default withStyles(styles)(App);
